#include "imgui_overlay.hpp"
#include <optional>
#include <stdexcept>
#include <dlfcn.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_keycode.h>
#include <SDL2/SDL_keyboard.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_sdl.h>
#include <pahil.hpp>

using namespace std::string_literals;

static int toggle_keysym;

static bool overlay_initialised = false;
static bool overlay_active = false;

void *hook_SDL_GL_SwapWindow(SDL_Window *window) {
    if(!overlay_active) {
        return NULL;
    }

    if(!overlay_initialised) {
        overlay_initialised = true;
        
        ImGui::CreateContext();

        auto gl_context = SDL_GL_GetCurrentContext();
        ImGui_ImplSDL2_InitForOpenGL(
            window, reinterpret_cast<void *>(gl_context));

        GLenum err = glewInit();
        if (err != GLEW_OK)
        {
        g_logCallback(
            "GLEW init failed! "s
            + reinterpret_cast<const char *>(glewGetErrorString(err)));
          return NULL;
        }

        ImGui_ImplOpenGL3_Init("#version 130");

        g_imguiInitFunc();
    }

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplSDL2_NewFrame(window);
    ImGui::NewFrame();

    g_imguiDrawFunc();

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    return NULL;
}

int (*orig_SDL_PollEvent)(SDL_Event *event) = NULL;

void *hook_SDL_PollEvent(SDL_Event *event) {
    static int zero = 0;
    static int one = 0;

    SDL_Event ev;
    while(orig_SDL_PollEvent(&ev)) {
        if(ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_F12) {
            overlay_active = !overlay_active;
        }
        if(!overlay_initialised) {
            *event = ev;
            return &one;
        }
        if(!ImGui_ImplSDL2_ProcessEvent(&ev)
                && ev.type != SDL_MOUSEMOTION) {
            *event = ev;
            return &one;
        }
    }
    return &zero;
}

int (*orig_SDL_PeepEvents)(
        SDL_Event *events, int numevents, SDL_eventaction action,
        uint32_t minType, uint32_t maxType) = NULL;

void *hook_SDL_PeepEvents(
        SDL_Event *events, int numevents, SDL_eventaction action,
        uint32_t minType, uint32_t maxType) {
    if(action == SDL_ADDEVENT) {
        return NULL;
    }

    std::vector<SDL_Event> evs(numevents);

    int ev_num = orig_SDL_PeepEvents(
        evs.data(), numevents, action, minType, maxType);

    static int events_i; // TODO: Without static?
    events_i = 0;
    for(size_t i = 0; i < static_cast<size_t>(ev_num); i++) {
        auto ev = evs[i];
        if(ev.type == SDL_KEYDOWN && ev.key.keysym.sym == SDLK_F12) {
            overlay_active = !overlay_active;
        }
        if(overlay_initialised && overlay_active) {
            if(ev.type == SDL_MOUSEMOTION) {
                // Just throwing these away lags the input processing thread,
                // so just send dummy ones instead
                // A proper fix would of course be better
                ev.motion.x = 0;
                ev.motion.xrel = 0;
                ev.motion.y = 0;
                ev.motion.yrel = 0;
                events[events_i] = ev;
                events_i++;
            } else if(!ImGui_ImplSDL2_ProcessEvent(&ev)) {
                events[events_i] = ev;
                events_i++;
            }
        } else {
            events[events_i] = ev;
            events_i++;
        }
    }

    return &events_i;
}

void imgui_overlay_opengl3_init() {
    toggle_keysym = SDL_GetKeyFromName(g_overlayToggle.c_str());
    if(toggle_keysym == SDLK_UNKNOWN) {
        g_logCallback("Unknown overlay toggle key, using F12");
        toggle_keysym = SDLK_F12;
    }

    void *swapwindow = dlsym(RTLD_DEFAULT, "SDL_GL_SwapWindow");
    if(swapwindow == NULL) {
        throw std::runtime_error(
            "Failed to dynamically load SDL_GL_SwapWindow");
    }
    void *pollevent = dlsym(RTLD_DEFAULT, "SDL_PollEvent");
    if(pollevent == NULL) {
        throw std::runtime_error(
            "Failed to dynamically load SDL_PollEvent");
    }
    void *peepevents = dlsym(RTLD_DEFAULT, "SDL_PeepEvents");
    if(peepevents == NULL) {
        throw std::runtime_error(
            "Failed to dynamically load SDL_PeepEvents");
    }

    std::string sdl_name;
    for(auto mod : pahil::Module::get_all_modules()) {
        if(mod.rfind("libSDL2-2.0.so") == 0) {
            sdl_name = mod;
            break;
        }
    }
    if(sdl_name.empty()) {
        throw std::runtime_error("Failed find SDL library");
    }

    pahil::Module sdl(0x0, sdl_name);
    sdl.unprotect_pages();
    sdl.init_exec_allocator();

    {
        auto p = sdl.patch();
        p.hook_real(swapwindow, reinterpret_cast<void *>(hook_SDL_GL_SwapWindow));
        p.hook_real(pollevent, reinterpret_cast<void *>(hook_SDL_PollEvent),
                    reinterpret_cast<void **>(&orig_SDL_PollEvent));
        p.hook_real(peepevents, reinterpret_cast<void *>(hook_SDL_PeepEvents),
                    reinterpret_cast<void **>(&orig_SDL_PeepEvents));
        p.apply();
    }
}
