#pragma once
#include <windows.h>
#include <d3d12.h>

EXTERN_C HRESULT WINAPI hook_CreateDXGIFactory(REFIID riid, void **ppFactory);
EXTERN_C HRESULT WINAPI hook_CreateDXGIFactory1(REFIID riid, void **ppFactory);
EXTERN_C HRESULT WINAPI hook_CreateDXGIFactory2(UINT flags, REFIID riid, void **ppFactory);
EXTERN_C HRESULT WINAPI hook_D3D12CreateDevice(
  IUnknown *pAdapter, D3D_FEATURE_LEVEL MinimumFeatureLevel,
  REFIID riid, void **ppDevice);