#pragma once
#include <Windows.h>

// Sets whether the overlay should consume inputs
extern bool win32_input_overlay_active;

void hook_win32_window(HWND hWnd);
