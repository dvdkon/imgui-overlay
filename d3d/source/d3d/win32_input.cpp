#include "win32_input.h"
#include <imgui_impl_win32.h>
#include "Logging/MessageLog.h"
#include "hook_manager.hpp"

// This forward declaration is needed, no idea why
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(
    HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

using namespace GameOverlay;

int win32_input_overlay_toggle = VK_F12;
bool win32_input_overlay_active = false;

static LRESULT WINAPI hook_WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
  switch (msg) {
    case WM_KEYDOWN:
    case WM_SYSKEYDOWN:
      if (wParam == win32_input_overlay_toggle) {
        win32_input_overlay_active = !win32_input_overlay_active;
        return true;
      }
    case WM_KEYUP:
    case WM_SYSKEYUP:
    case WM_INPUT:
    case WM_CHAR:
    case WM_DEADCHAR:
    case WM_SETCURSOR:
    case WM_MOUSEWHEEL:
    case WM_MOUSEHWHEEL:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    case WM_MBUTTONUP:
    case WM_XBUTTONUP:
    case WM_LBUTTONDOWN:
    case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN:
    case WM_RBUTTONDBLCLK:
    case WM_MBUTTONDOWN:
    case WM_MBUTTONDBLCLK:
    case WM_XBUTTONDOWN:
    case WM_XBUTTONDBLCLK:
      // Don't ever pass input events to the hooked app if the overlay is active
      if (win32_input_overlay_active) {
        ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam);
        return true;
      }
  }
  if (win32_input_overlay_active) {
    if (ImGui_ImplWin32_WndProcHandler(hWnd, msg, wParam, lParam)) {
      return true;
    }
  }

  return find_hook_trampoline(hook_WndProc)(hWnd, msg, wParam, lParam);
}

void hook_win32_window(HWND hWnd)
{
  // We don't need to handle strings in hook_WndProc, so a single function
  // is used for both Unicode and ASCII
  hook::address wndproc;
  if (IsWindowUnicode(hWnd)) {
    wndproc = reinterpret_cast<hook::address>(GetWindowLongPtrW(hWnd, GWLP_WNDPROC));
  }
  else {
    wndproc = reinterpret_cast<hook::address>(GetWindowLongPtrA(hWnd, GWLP_WNDPROC));
  }
  install_hook(wndproc, hook_WndProc);

  g_messageLog.LogInfo("win32_input", "Successfully hooked Win32 input functions.");
}
