// Copyright 2016 Patrick Mours.All rights reserved.
//
// https://github.com/crosire/gameoverlay
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met :
//
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT
// SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

#include "d3d11_renderer.hpp"
#include <assert.h>
#include <vector>
#include "imgui.h"
#include "imgui_impl_dx11.h"
#include "imgui_impl_win32.h"

#include "Logging/MessageLog.h"
#include "imgui_overlay_d3d.hpp"

#include "win32_input.h"

using Microsoft::WRL::ComPtr;

namespace GameOverlay {
d3d11_renderer::d3d11_renderer(ID3D11Device* device, IDXGISwapChain* swapchain)
    : device_(device), swapchain_(swapchain)
{
  g_messageLog.LogInfo("D3D11", "Initializing overlay.");

  DXGI_SWAP_CHAIN_DESC swapchain_desc;
  swapchain_->GetDesc(&swapchain_desc);

  device_->GetImmediateContext(&context_);

  if (!InitImgui()) {
    return;
  }

  status = InitializationStatus::DEFERRED_CONTEXT_INITIALIZED;
  g_messageLog.LogInfo("D3D11", "Overlay successfully initialized.");
}

d3d11_renderer::d3d11_renderer(
    ID3D11Device* device, std::vector<Microsoft::WRL::ComPtr<ID3D11RenderTargetView>> renderTargets,
    int backBufferWidth, int backBufferHeight)
    : device_(device), renderTargets_(renderTargets)
{
  device_->GetImmediateContext(&context_);

  if (!InitImgui()) {
    return;
  }

  status = InitializationStatus::DEFERRED_CONTEXT_INITIALIZED;
  g_messageLog.LogInfo("D3D11", "Overlay successfully initialized.");
}

d3d11_renderer::~d3d11_renderer()
{
  // Empty
}

bool d3d11_renderer::InitImgui()
{
  DXGI_SWAP_CHAIN_DESC swapchain_desc;
  swapchain_->GetDesc(&swapchain_desc);
  RECT rect;
  GetWindowRect(swapchain_desc.OutputWindow, &rect);

  const auto width = static_cast<float>(rect.right - rect.left);
  const auto height = static_cast<float>(rect.bottom - rect.top);

  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  io.DisplaySize.x = width;
  io.DisplaySize.y = height;
  // io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
  // io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

  ImGui_ImplDX11_Init(device_.Get(), context_.Get());

  ImGui_ImplWin32_Init(swapchain_desc.OutputWindow);
  ImGui_ImplWin32_EnableDpiAwareness();
  hook_win32_window(swapchain_desc.OutputWindow);

  g_imguiInitFunc();

  return true;
}

// just use first render target, probably we only have one here anyways
bool d3d11_renderer::on_present(bool lagIndicatorState) { return on_present(0, lagIndicatorState); }

bool d3d11_renderer::on_present(int backBufferIndex, bool lagIndicatorState)
{
  if (status == InitializationStatus::UNINITIALIZED) {
    return false;
  }

  if (status == InitializationStatus::IMMEDIATE_CONTEXT_INITIALIZED) {
    g_messageLog.LogError("D3D11", "D3D11 without deferred context doesn't work for now");
    return false;
  }

  if (win32_input_overlay_active) {
    ImGui_ImplDX11_NewFrame();
    ImGui_ImplWin32_NewFrame();
    ImGui::NewFrame();

    g_imguiDrawFunc();
    
    ImGui::Render();
    ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

    return true;
  }

  return false;
}

}  // namespace GameOverlay
