// Copyright 2016 Patrick Mours.All rights reserved.
//
// https://github.com/crosire/gameoverlay
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met :
//
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT
// SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

#include "d3d12_renderer.hpp"
#include <assert.h>
#include "../deps/d3dx12/d3dx12.h"
#include "win32_input.h"
#include "imgui_overlay_d3d.hpp"
#include "Logging/MessageLog.h"

#include "imgui.h"
#include "imgui_impl_dx12.h"
#include "imgui_impl_win32.h"

using Microsoft::WRL::ComPtr;

namespace GameOverlay {
d3d12_renderer::d3d12_renderer(ID3D12CommandQueue* commandqueue, IDXGISwapChain3* swapchain)
    : queue_(commandqueue), swapchain_(swapchain)
{
  g_messageLog.LogInfo("D3D12", "Initializing overlay.");
  queue_->GetDevice(IID_PPV_ARGS(&device_));

  DXGI_SWAP_CHAIN_DESC1 swapchain_desc;
  swapchain_->GetDesc1(&swapchain_desc);

  bufferCount_ = swapchain_desc.BufferCount;

  if (!CreateFrameFences()) return;
  if (!CreateCMDList()) return;
  if (!CreateRenderTargets()) return;
  if (!CreateDisplayHeap()) return;
  if (!InitImgui()) return;

  initSuccessfull_ = true;
  g_messageLog.LogInfo("D3D12", "Overlay successfully initialized.");
}

d3d12_renderer::d3d12_renderer(ID3D12CommandQueue* commandqueue,
                               Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> renderTargetHeap,
                               std::vector<Microsoft::WRL::ComPtr<ID3D12Resource>> renderTargets,
                               UINT rtvHeapDescriptorSize, int bufferCount, int backBufferWidth,
                               int backBufferHeight)
    : queue_(commandqueue),
      renderTargetHeap_(renderTargetHeap),
      renderTargets_(renderTargets),
      rtvHeapDescriptorSize_(rtvHeapDescriptorSize),
      bufferCount_(bufferCount)
{
  queue_->GetDevice(IID_PPV_ARGS(&device_));
  if (!CreateFrameFences()) return;
  if (!CreateCMDList()) return;
  if (!CreateDisplayHeap()) return;
  if (!InitImgui()) return;

  initSuccessfull_ = true;
  g_messageLog.LogInfo("D3D12", "Overlay successfully initialized.");
}

d3d12_renderer::~d3d12_renderer()
{
  for (auto handle : frameFenceEvents_) {
    CloseHandle(handle);
  }
}

namespace {
void WaitForFence(ID3D12Fence* fence, UINT64 completionValue, HANDLE waitEvent)
{
  if (fence->GetCompletedValue() < completionValue) {
    fence->SetEventOnCompletion(completionValue, waitEvent);
    WaitForSingleObject(waitEvent, INFINITE);
  }
}
}  // namespace

bool d3d12_renderer::on_present()
{
  const auto currentBufferIndex = swapchain_->GetCurrentBackBufferIndex();
  return on_present(currentBufferIndex);
}

bool d3d12_renderer::on_present(int backBufferIndex)
{
  if (!initSuccessfull_) {
    return false;
  }

  WaitForFence(frameFences_[backBufferIndex].Get(), frameFenceValues_[backBufferIndex],
               frameFenceEvents_[backBufferIndex]);

  commandPool_->Reset();

  if (win32_input_overlay_active) {
    DrawOverlay(backBufferIndex);
  }

  return true;
}

void d3d12_renderer::UpdateOverlayPosition()
{
  DXGI_SWAP_CHAIN_DESC1 swapchain_desc;
  swapchain_->GetDesc1(&swapchain_desc);

  // update the viewport
  viewPort_.TopLeftX = 0;
  viewPort_.TopLeftY = 0;
  viewPort_.Width = static_cast<float>(swapchain_desc.Width);
  viewPort_.Height = static_cast<float>(swapchain_desc.Height);
  viewPort_.MaxDepth = 1.0f;
  viewPort_.MinDepth = 0.0f;

  rectScissor_.left = 0;
  rectScissor_.top = 0;
  rectScissor_.right = swapchain_desc.Width;
  rectScissor_.bottom = swapchain_desc.Height;
}

bool d3d12_renderer::CreateCMDList()
{
  commandPool_.Reset();
  commandList_.Reset();

  HRESULT hr;
  hr = device_->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandPool_));
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "CreateCommandAllocator failed", hr);
    return false;
  }

  hr = device_->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandPool_.Get(), nullptr,
                                  IID_PPV_ARGS(&commandList_));
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "CreateCommandList failed", hr);
    return false;
  }

  commandList_->Close();

  return true;
}

bool d3d12_renderer::CreateRenderTargets()
{
  D3D12_DESCRIPTOR_HEAP_DESC rtvHeapDesc = {};
  rtvHeapDesc.NumDescriptors = bufferCount_;
  rtvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
  rtvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
  HRESULT hr = device_->CreateDescriptorHeap(&rtvHeapDesc, IID_PPV_ARGS(&renderTargetHeap_));
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "CreateRenderTargets - CreateDescriptorHeap failed", hr);
    return false;
  }

  rtvHeapDescriptorSize_ =
    device_->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
  CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(renderTargetHeap_->GetCPUDescriptorHandleForHeapStart());
  renderTargets_.resize(bufferCount_);

  // Create a RTV for each frame.
  for (UINT buffer_index = 0; buffer_index < renderTargets_.size(); buffer_index++) {
    hr = swapchain_->GetBuffer(buffer_index, IID_PPV_ARGS(&renderTargets_[buffer_index]));
    if (FAILED(hr)) {
      g_messageLog.LogError("D3D12", "CreateRenderTargets - GetBuffer failed", hr);
      return false;
    }
    device_->CreateRenderTargetView(renderTargets_[buffer_index].Get(), nullptr, rtvHandle);
    rtvHandle.Offset(1, rtvHeapDescriptorSize_);
  }
  return true;
}

bool d3d12_renderer::CreateFrameFences()
{
  currFenceValue_ = 0;

  frameFences_.resize(bufferCount_);
  for (int i = 0; i < bufferCount_; ++i) {
    HRESULT hr = device_->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&frameFences_[i]));
    if (FAILED(hr)) {
      g_messageLog.LogError("D3D12", "Create Fence failed", hr);
      return false;
    }
    frameFenceEvents_.push_back(CreateEvent(nullptr, FALSE, FALSE, nullptr));
    frameFenceValues_.push_back(0);
  }
  return true;
}

bool d3d12_renderer::CreateDisplayHeap()
{
  commandList_->Reset(commandPool_.Get(), nullptr);

  HRESULT hr;

  // create srv
  D3D12_DESCRIPTOR_HEAP_DESC srvHeapDesc = {};
  srvHeapDesc.NumDescriptors = 2;
  srvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
  srvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
  hr = device_->CreateDescriptorHeap(&srvHeapDesc, IID_PPV_ARGS(&displayHeap_));
  if (FAILED(hr)) {
      g_messageLog.LogError("D3D12", "CreateDisplayHeap - CreateDescriptorHeap displayHeap_",
                          hr);
      return false;
  }

  // execute command list
  hr = commandList_->Close();
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "CreateDisplayHeap - Failed closing CommandList", hr);
    return false;
  }
  ID3D12CommandList* ppCommandLists[] = {commandList_.Get()};
  queue_->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

  WaitForCompletion();
  return true;
}


bool d3d12_renderer::InitImgui() {
  DXGI_SWAP_CHAIN_DESC swapchain_desc;
  swapchain_->GetDesc(&swapchain_desc);
  DXGI_SWAP_CHAIN_DESC1 swapchain_desc1;
  swapchain_->GetDesc1(&swapchain_desc1);

  IMGUI_CHECKVERSION();
  ImGui::CreateContext();
  ImGuiIO& io = ImGui::GetIO();
  io.DisplaySize.x = static_cast<float>(swapchain_desc1.Width);
  io.DisplaySize.y = static_cast<float>(swapchain_desc1.Height);
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

  ImGui_ImplDX12_Init(device_.Get(), bufferCount_, DXGI_FORMAT_R8G8B8A8_UNORM,
                      displayHeap_.Get(),
                      displayHeap_->GetCPUDescriptorHandleForHeapStart(),
                      displayHeap_->GetGPUDescriptorHandleForHeapStart());
  
  ImGui_ImplWin32_Init(swapchain_desc.OutputWindow);
  ImGui_ImplWin32_EnableDpiAwareness();
  hook_win32_window(swapchain_desc.OutputWindow);

  g_imguiInitFunc();

  return true;
}

void d3d12_renderer::DrawOverlay(int currentIndex)
{
  HRESULT hr = commandList_->Reset(commandPool_.Get(), nullptr);
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "DrawOverlay - Failed to reset command list.", hr);
    return;
  }

  ImGui_ImplDX12_NewFrame();
  ImGui_ImplWin32_NewFrame();
  ImGui::NewFrame();

  g_imguiDrawFunc();

  const auto transitionRender = CD3DX12_RESOURCE_BARRIER::Transition(
    renderTargets_[currentIndex].Get(), D3D12_RESOURCE_STATE_PRESENT,
    D3D12_RESOURCE_STATE_RENDER_TARGET, 0);
  commandList_->ResourceBarrier(1, &transitionRender);

  CD3DX12_CPU_DESCRIPTOR_HANDLE rtvHandle(renderTargetHeap_->GetCPUDescriptorHandleForHeapStart(),
                                          currentIndex, rtvHeapDescriptorSize_);
  commandList_->OMSetRenderTargets(1, &rtvHandle, FALSE, nullptr);

  ImGui::Render();
  ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), commandList_.Get());


  const auto transitionPresent = CD3DX12_RESOURCE_BARRIER::Transition(
    renderTargets_[currentIndex].Get(), D3D12_RESOURCE_STATE_RENDER_TARGET,
    D3D12_RESOURCE_STATE_PRESENT, 0);
  commandList_->ResourceBarrier(1, &transitionPresent);

  hr = commandList_->Close();
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "DrawOverlay - Failed to close command list.", hr);
    return;
  }

  ID3D12CommandList* commandLists[] = {commandList_.Get()};
  queue_->ExecuteCommandLists(1, commandLists);

  queue_->Signal(frameFences_[currentIndex].Get(), currFenceValue_);
  frameFenceValues_[currentIndex] = currFenceValue_;
  ++currFenceValue_;
}

void d3d12_renderer::WaitForCompletion()
{
  HRESULT hr = device_->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence_));
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "WaitForCompletion - CreateFence failed", hr);
    return;
  }
  fenceValue_ = 1;

  fenceEvent_ = CreateEvent(nullptr, FALSE, FALSE, nullptr);
  if (fenceEvent_ == nullptr) {
    hr = HRESULT_FROM_WIN32(GetLastError());
    if (FAILED(hr)) {
      g_messageLog.LogError("D3D12", "WaitForCompletion - CreateEvent failed", hr);
      return;
    }
  }

  const auto currFenceValue = fenceValue_;
  hr = queue_->Signal(fence_.Get(), currFenceValue);
  if (FAILED(hr)) {
    g_messageLog.LogError("D3D12", "WaitForCompletion - Signal queue failed", hr);
    return;
  }

  if (fence_->GetCompletedValue() < currFenceValue) {
    hr = fence_->SetEventOnCompletion(currFenceValue, fenceEvent_);
    WaitForSingleObject(fenceEvent_, INFINITE);
  }
}
}  // namespace GameOverlay
