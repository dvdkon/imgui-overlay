// Copyright 2016 Patrick Mours.All rights reserved.
//
// https://github.com/crosire/gameoverlay
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met :
//
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT
// SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

#include "hook_manager.hpp"
#include <tlhelp32.h>

#include <assert.h>
#include <algorithm>
#include <unordered_map>
#include <vector>
#include "Logging/MessageLog.h"
#include "critical_section.hpp"

namespace GameOverlay {
  namespace
  {
    enum class hook_method
    {
      function_hook,
      vtable_hook
    };

    struct module_export
    {
      hook::address address;
      const char *name;
      unsigned short ordinal;
    };

    HMODULE get_current_module()
    {
      HMODULE handle = nullptr;
      GetModuleHandleExW(
        GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS | GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        reinterpret_cast<LPCWSTR>(&get_current_module), &handle);

      return handle;
    }
    std::vector<module_export> get_module_exports(HMODULE handle)
    {
      std::vector<module_export> exports;
      const auto imagebase = reinterpret_cast<const BYTE *>(handle);
      const auto imageheader = reinterpret_cast<const IMAGE_NT_HEADERS *>(
        imagebase + reinterpret_cast<const IMAGE_DOS_HEADER *>(imagebase)->e_lfanew);

      if (imageheader->Signature != IMAGE_NT_SIGNATURE ||
        imageheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size == 0) {
        return exports;
      }

      const auto exportdir = reinterpret_cast<const IMAGE_EXPORT_DIRECTORY *>(
        imagebase +
        imageheader->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress);
      const auto exportbase = static_cast<WORD>(exportdir->Base);

      if (exportdir->NumberOfFunctions == 0) {
        return exports;
      }

      const auto count = static_cast<size_t>(exportdir->NumberOfNames);
      exports.reserve(count);

      for (size_t i = 0; i < count; ++i) {
        module_export symbol;
        symbol.ordinal =
          reinterpret_cast<const WORD *>(imagebase + exportdir->AddressOfNameOrdinals)[i] +
          exportbase;
        symbol.name = reinterpret_cast<const char *>(
          imagebase + reinterpret_cast<const DWORD *>(imagebase + exportdir->AddressOfNames)[i]);
        symbol.address = const_cast<void *>(reinterpret_cast<const void *>(
          imagebase + reinterpret_cast<const DWORD *>(
            imagebase + exportdir->AddressOfFunctions)[symbol.ordinal - exportbase]));

        exports.push_back(std::move(symbol));
      }

      return exports;
    }

    critical_section s_cs;
    std::vector<std::wstring> s_delayed_hook_paths;
    std::vector<HMODULE> s_delayed_hook_modules;
    std::vector<std::pair<hook, hook_method>> s_hooks;
    std::unordered_map<hook::address, hook::address *> s_vtable_addresses;

    bool install_hook(hook::address target, hook::address replacement, hook_method method)
    {
      hook hook(target, replacement);
      hook.trampoline = target;

      hook::status status = hook::status::unknown;

      switch (method) {
      case hook_method::function_hook: {
        status = hook.install();
        break;
      }
      case hook_method::vtable_hook: {
        DWORD protection = PAGE_READWRITE;
        const auto target_address = s_vtable_addresses.at(target);

        if (VirtualProtect(target_address, sizeof(*target_address), protection, &protection)) {
          *target_address = replacement;

          VirtualProtect(target_address, sizeof(*target_address), protection, &protection);

          status = hook::status::success;
        }
        else {
          status = hook::status::memory_protection_failure;
        }
        break;
      }
      }

      if (status != hook::status::success) {
        g_messageLog.LogVerbose("install_hook", "Hook installation failed");
        return false;
      }

      const critical_section::lock lock(s_cs);

      s_hooks.emplace_back(std::move(hook), method);

      g_messageLog.LogVerbose("install_hook", "Successfully installed hook");
      return true;
    }
    bool install_hook(const HMODULE target_module, const HMODULE replacement_module, hook_method method)
    {
      assert(target_module != nullptr);
      assert(replacement_module != nullptr);

      // Load export tables
      const auto target_exports = get_module_exports(target_module);
      const auto replacement_exports = get_module_exports(replacement_module);

      if (target_exports.empty()) {
        g_messageLog.LogVerbose("install_hook", "No exports found");
        return false;
      }

      size_t install_count = 0;
      std::vector<std::pair<hook::address, hook::address>> matches;
      matches.reserve(replacement_exports.size());

      // Analyze export table
      for (const auto &symbol : target_exports) {
        if (symbol.name == nullptr || symbol.address == nullptr) {
          continue;
        }

        // Find appropriate replacement
        const auto it = std::find_if(replacement_exports.cbegin(), replacement_exports.cend(),
          [&symbol](const module_export &moduleexport) {
          return std::strcmp(moduleexport.name, symbol.name) == 0;
        });

        if (it == replacement_exports.cend()) {
          continue;
        }
        g_messageLog.LogVerbose("install_hook", "Found matching function: " + std::string(symbol.name));
        matches.push_back(std::make_pair(symbol.address, it->address));
      }

      // Hook matching exports
      for (const auto &match : matches) {
        if (install_hook(match.first, match.second, method)) {
          install_count++;
        }
      }
      g_messageLog.LogVerbose("install_hook", "Install count: " + std::to_string(install_count));
      return install_count != 0;
    }
    bool uninstall_hook(hook &hook, hook_method method)
    {
      if (hook.uninstalled()) {
        return true;
      }

      hook::status status = hook::status::unknown;

      switch (method) {
      case hook_method::function_hook: {
        status = hook.uninstall();
        break;
      }
      case hook_method::vtable_hook: {
        DWORD protection = PAGE_READWRITE;
        const auto target_address = s_vtable_addresses.at(hook.target);

        if (VirtualProtect(target_address, sizeof(*target_address), protection, &protection)) {
          *target_address = hook.target;
          s_vtable_addresses.erase(hook.target);

          VirtualProtect(target_address, sizeof(*target_address), protection, &protection);

          status = hook::status::success;
        }
        else {
          status = hook::status::memory_protection_failure;
        }
        break;
      }
      }

      if (status != hook::status::success) {
        return false;
      }

      hook.trampoline = nullptr;

      return true;
    }
    bool replace_hook(const HMODULE target_module, const HMODULE replacement_module, hook_method method)
    {
      assert(target_module != nullptr);
      assert(replacement_module != nullptr);

      // Load export tables
      const auto target_exports = get_module_exports(target_module);
      const auto replacement_exports = get_module_exports(replacement_module);

      if (target_exports.empty()) {
        g_messageLog.LogVerbose("install_hook", "No exports found");
        return false;
      }

      size_t install_count = 0;
      std::vector<std::pair<hook::address, hook::address>> matches;
      matches.reserve(replacement_exports.size());

      // Analyze export table
      for (const auto &symbol : target_exports) {
        if (symbol.name == nullptr || symbol.address == nullptr) {
          continue;
        }

        // Find appropriate replacement
        const auto it = std::find_if(replacement_exports.cbegin(), replacement_exports.cend(),
          [&symbol](const module_export &moduleexport) {
          return std::strcmp(moduleexport.name, symbol.name) == 0;
        });

        if (it == replacement_exports.cend()) {
          continue;
        }
        g_messageLog.LogVerbose("install_hook", "Found matching function: " + std::string(symbol.name));
        matches.push_back(std::make_pair(symbol.address, it->address));
      }

      // uninstall in case there exist already a hook
      for (const auto &match : matches) {
        hook hook(match.first, match.second);
        hook.trampoline = match.first;
        uninstall_hook(hook, method);
        // install new hook
        if (install_hook(match.first, match.second, method)) {
          install_count++;
        }
      }
      g_messageLog.LogVerbose("install_hook", "Install count: " + std::to_string(install_count));
      return install_count != 0;
    }
    hook find_hook(hook::address replacement)
    {
      const critical_section::lock lock(s_cs);

      const auto it = std::find_if(s_hooks.cbegin(), s_hooks.cend(),
        [replacement](const std::pair<hook, hook_method> &hook) {
        return hook.first.replacement == replacement;
      });

      if (it == s_hooks.cend()) {
        return hook();
      }

      return it->first;
    }
    template <typename T>
    inline T find_hook_trampoline_unchecked(T replacement)
    {
      return reinterpret_cast<T>(find_hook(reinterpret_cast<hook::address>(replacement)).call());
    }
    std::wstring GetProcessName(LPCTSTR lpApplicationName, LPTSTR lpCommandLine)
    {
      std::wstring path;
      size_t processStart = 0;
      size_t processSize = 0;
      if (lpApplicationName) {
        path = std::wstring(lpApplicationName);
        processStart = path.find_last_of('\\') + 1;
        processSize = path.size() - processStart;
      }
      else if (lpCommandLine) {
        path = std::wstring(lpCommandLine);
        const auto exePathEnd = path.find_first_of(L'"', 1);
        processStart = path.find_last_of('\\', exePathEnd) + 1;
        processSize = exePathEnd - processStart;
      }

      return path.substr(processStart, processSize);
    }
  }

  bool install_hook(hook::address target, hook::address replacement)
  {
    assert(target != nullptr);
    assert(replacement != nullptr);

    if (target == replacement) {
      g_messageLog.LogVerbose("install_hook", "Target module equals replacement.");
      return false;
    }

    const hook hook = find_hook(replacement);

    if (hook.installed()) {
      bool success = target == hook.target;
      if (success) {
        g_messageLog.LogVerbose("install_hook", "Hook already installed");
      }
      else {
        g_messageLog.LogVerbose("install_hook", "There exists another module with the same name but a different address");
      }
      return success;
    }

    g_messageLog.LogVerbose("install_hook", "Try install");
    return install_hook(target, replacement, hook_method::function_hook);
  }

  bool install_hook(hook::address vtable[], unsigned int offset, hook::address replacement)
  {
    assert(vtable != nullptr);
    assert(replacement != nullptr);

    DWORD protection = PAGE_READONLY;
    hook::address &target = vtable[offset];

    if (VirtualProtect(&target, sizeof(hook::address), protection, &protection)) {
      const critical_section::lock lock(s_cs);

      const auto insert = s_vtable_addresses.emplace(target, &target);

      VirtualProtect(&target, sizeof(hook::address), protection, &protection);

      if (insert.second) {
        if (target != replacement && install_hook(target, replacement, hook_method::vtable_hook)) {
          return true;
        }

        s_vtable_addresses.erase(insert.first);
      }
      else {
        return insert.first->first == target;
      }
    }

    return false;
  }
  void uninstall_hook()
  {
    const critical_section::lock lock(s_cs);

    // Uninstall hooks
    for (auto &hook : s_hooks) {
      uninstall_hook(hook.first, hook.second);
    }

    s_hooks.clear();

    // Free loaded modules
    for (HMODULE module : s_delayed_hook_modules) {
      FreeLibrary(module);
    }

    s_delayed_hook_modules.clear();
  }

  __declspec(dllexport) bool replace_vtable_hook(hook::address vtable[], unsigned int offset, hook::address replacement)
  {
    assert(vtable != nullptr);
    assert(replacement != nullptr);

    DWORD protection = PAGE_READONLY;
    hook::address &target = vtable[offset];
    auto hook = find_hook(target);
    if (hook.target) {
      uninstall_hook(hook, hook_method::vtable_hook);
    }

    if (VirtualProtect(&target, sizeof(hook::address), protection, &protection)) {
      const critical_section::lock lock(s_cs);

      const auto insert = s_vtable_addresses.emplace(target, &target);

      VirtualProtect(&target, sizeof(hook::address), protection, &protection);

      if (insert.second) {
        if (target != replacement) {
          
          if (install_hook(target, replacement, hook_method::vtable_hook)) {
            return true;
          }
        }

        s_vtable_addresses.erase(insert.first);
      }
      else {
        return insert.first->first == target;
      }
    }

    return false;
  }
  __declspec(dllexport) hook::address find_hook_trampoline(hook::address replacement)
  {
    const hook hook = find_hook(replacement);

    if (!hook.valid()) {
      return nullptr;
    }

    return hook.call();
  }
}
