// Copyright 2016 Patrick Mours.All rights reserved.
//
// https://github.com/crosire/gameoverlay
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met :
//
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and / or other materials provided with the distribution.
//
// THIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.IN NO EVENT
// SHALL COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES(INCLUDING, BUT NOT
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
// WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT(INCLUDING NEGLIGENCE
// OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
// OF THE POSSIBILITY OF SUCH DAMAGE.

#include "imgui_overlay.hpp"
#include <windows.h>
#include <appmodel.h>
#include <dxgi1_3.h>
#include <psapi.h>
#include <assert.h>
#include "hook_manager.hpp"
#include "Logging/MessageLog.h"
#include "d3d/dxgi_hook.hpp"
#include "d3d/win32_input.h"

using namespace GameOverlay;

#pragma data_seg("SHARED")
HWND sharedFrontendWindow = NULL;
#pragma data_seg()
#pragma comment(linker, "/section:SHARED,RWS")

extern "C" __declspec(dllexport) LRESULT CALLBACK
    GlobalHookProc(int code, WPARAM wParam, LPARAM lParam)
{
  return CallNextHookEx(NULL, code, wParam, lParam);
}

typedef LONG(WINAPI *PGetPackageFamilyName)(HANDLE, UINT32 *, PWSTR);

void imgui_overlay_d3d_init()
{
  g_imguiInitFunc = imguiInitFunc;
  g_imguiDrawFunc = imguiDrawFunc;

  g_messageLog.Start(g_logCallback);
  g_messageLog.LogInfo("GameOverlay", "ImGui Overlay (D3D) started!");

  HMODULE dxgi = GetModuleHandle(L"dxgi.dll");
  if(dxgi == NULL) {
    g_messageLog.LogError("GameOverlay", "Failed to get handle to dxgi.dll", GetLastError());
  }

  if(!install_hook(GetProcAddress(dxgi, "CreateDXGIFactory"), hook_CreateDXGIFactory)) {
    return;
    g_messageLog.LogInfo("GameOverlay", "Hooking CreateDXGIFactory failed");
  }
  if(!install_hook(GetProcAddress(dxgi, "CreateDXGIFactory1"), hook_CreateDXGIFactory1)) {
    return;
    g_messageLog.LogInfo("GameOverlay", "Hooking CreateDXGIFactory1 failed");
  }
  if(!install_hook(GetProcAddress(dxgi, "CreateDXGIFactory2"), hook_CreateDXGIFactory2)) {
    return;
    g_messageLog.LogInfo("GameOverlay", "Hooking CreateDXGIFactory2 failed");
  }

  HMODULE d3d12 = GetModuleHandle(L"d3d12.dll");
  if(d3d12 == NULL) {
    g_messageLog.LogInfo("GameOverlay", "Failed to get handle to d3d12.dll, continuing anyway...");
  } else {
    if(!install_hook(GetProcAddress(d3d12, "D3D12CreateDevice"), hook_D3D12CreateDevice)) {
      return;
      g_messageLog.LogInfo("GameOverlay", "Hooking D3D12CreateDevice failed");
    }
  }

}
