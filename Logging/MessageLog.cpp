//
// Copyright(c) 2016 Advanced Micro Devices, Inc. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

#include "MessageLog.h"

#include <windows.h>
#include <sstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <locale>
#include <codecvt>

MessageLog::MessageLog()
  : filter_({ LogLevel::Error, LogLevel::Info, LogLevel::Warning }),
  started_(false)
{
}

MessageLog::~MessageLog()
{
}

void MessageLog::Start(LogEntryCallback logCallback)
{
  callback_ = logCallback;
  started_ = true;
  LogInfo("MessageLog", "Logging started");
}

void MessageLog::LogError(const std::string & category, const std::string & message, DWORD errorCode)
{
  Log(LogLevel::Error, category, message, errorCode);
}

void MessageLog::LogError(const std::string & category, const std::wstring & message, DWORD errorCode)
{
  Log(LogLevel::Error, category, message, errorCode);
}

void MessageLog::LogWarning(const std::string & category, const std::string & message, DWORD errorCode)
{
  Log(LogLevel::Warning, category, message, errorCode);
}

void MessageLog::LogWarning(const std::string & category, const std::wstring & message, DWORD errorCode)
{
  Log(LogLevel::Warning, category, message, errorCode);
}

void MessageLog::LogInfo(const std::string & category, const std::string & message, DWORD errorCode)
{
  Log(LogLevel::Info, category, message, errorCode);
}

void MessageLog::LogInfo(const std::string & category, const std::wstring & message, DWORD errorCode)
{
  Log(LogLevel::Info, category, message, errorCode);
}

void MessageLog::LogVerbose(const std::string & category, const std::string & message, DWORD errorCode)
{
  Log(LogLevel::Verbose, category, message, errorCode);
}

void MessageLog::LogVerbose(const std::string & category, const std::wstring & message, DWORD errorCode)
{
  Log(LogLevel::Verbose, category, message, errorCode);
}

void MessageLog::Log(LogLevel logLevel, const std::string& category, const std::string& message,
  DWORD errorCode)
{
  auto messageW =  std::wstring_convert<std::codecvt_utf8<wchar_t>>().from_bytes(message);
  Log(logLevel, category, messageW, errorCode);
}

void MessageLog::Log(LogLevel logLevel, const std::string& category, const std::wstring& messageW,
  DWORD errorCode)
{
  auto levelW = (logLevelNames_[static_cast<int>(logLevel)]);
  auto level =  std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(levelW);
  auto message = std::wstring_convert<std::codecvt_utf8<wchar_t>>().to_bytes(messageW);

  std::ostringstream combined;
  combined << level << " " << category << ": " << message;
  if(errorCode != 0) {
      combined << "(" << std::hex << errorCode << ")";
  }
  callback_(combined.str());
}

typedef void(WINAPI* FGETSYSTEMINFO)(LPSYSTEM_INFO);
typedef BOOL(WINAPI* FGETPRODUCTINFO)(DWORD, DWORD, DWORD, DWORD, PDWORD);
void MessageLog::LogOS()
{
  SYSTEM_INFO systemInfo = {};
  FGETSYSTEMINFO fGetSystemInfo = reinterpret_cast<FGETSYSTEMINFO>(
    GetProcAddress(GetModuleHandle(L"kernel32.dll"), "GetNativeSystemInfo"));
  if (fGetSystemInfo)
  {
    fGetSystemInfo(&systemInfo);
  }
  else {
    LogWarning("MessageLog", "LogOS: Unable to get address for GetNativeSystemInfo using GetSystemInfo");
    GetSystemInfo(&systemInfo);
  }

  std::string processorArchitecture = "Processor architecture: ";
  switch (systemInfo.wProcessorArchitecture) {
  case PROCESSOR_ARCHITECTURE_AMD64:
    processorArchitecture += "x64";
    break;
  case PROCESSOR_ARCHITECTURE_INTEL:
    processorArchitecture += "x86";
    break;
  case PROCESSOR_ARCHITECTURE_ARM:
    processorArchitecture += "ARM";
    break;
  case PROCESSOR_ARCHITECTURE_IA64:
    processorArchitecture += "Intel Itanium";
    break;
  default:
    processorArchitecture += "Unknown";
    break;
  }

  FGETPRODUCTINFO fGetProductInfo = reinterpret_cast<FGETPRODUCTINFO>(
    GetProcAddress(GetModuleHandle(L"kernel32.dll"), "GetProductInfo"));
  if (!fGetProductInfo)
  {
    LogError("MessageLog", "LogOS: Unable to get address for GetProductInfo");
    return;
  }

  DWORD type = 0;
  fGetProductInfo(6, 0, 0, 0, &type);
  const std::string osInfo = "OS: type: " + std::to_string(type);

  LogInfo("MessageLog", osInfo + " " + processorArchitecture);
}
