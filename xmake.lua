add_repositories("dvdkon-repo git@gitlab.com:dvdkon/xmake-repo.git")
add_requires("imgui")
set_languages("c++17")

if is_plat("windows") then
	add_requires("minhook")
elseif is_plat("linux") then
	add_requires("pahil")
end

target("imgui-overlay")
    set_kind("static")
	add_includedirs(".")
	add_headerfiles("imgui_overlay.hpp")
	add_files("imgui_overlay.cpp")
    add_packages("imgui")
	if is_plat("windows") then
		add_includedirs("d3d/deps", "d3d/source/")
		add_headerfiles("d3d/source/imgui_overlay_d3d.hpp")
		add_files("d3d/**.cpp")
		add_files("Logging/**.cpp")
		add_packages("minhook")
		add_defines("UNICODE")
	elseif is_plat("linux") then
		add_files("opengl/**.cpp")
		add_syslinks("sdl2")
		add_packages("pahil")
		add_cxflags("-fPIC")
	end
