#include "imgui_overlay.hpp"

std::string g_overlayToggle;
std::function<void(std::string)> g_logCallback;
std::function<void()> g_imguiInitFunc;
std::function<void()> g_imguiDrawFunc;

void imgui_overlay_init(
        std::string overlay_toggle,
        std::function<void(std::string)> log_callback,
        std::function<void()> init_imgui,
        std::function<void()> draw_imgui) {
    g_overlayToggle = overlay_toggle;
    g_logCallback = log_callback;
    g_imguiInitFunc = init_imgui;
    g_imguiDrawFunc = draw_imgui;
}
