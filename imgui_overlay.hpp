#pragma once
#include <string>
#include <functional>

extern std::string g_overlayToggle;
extern std::function<void(std::string)> g_logCallback;
extern std::function<void()> g_imguiInitFunc;
extern std::function<void()> g_imguiDrawFunc;

void imgui_overlay_init(
    std::string overlay_toggle,
    std::function<void(std::string)> log_callback,
    std::function<void()> init_imgui,
    std::function<void()> draw_imgui);

#if defined(_WIN32)
void imgui_overlay_d3d_init();
#endif

// Linux-only for now, but it should be easily portable to Windows
#if defined(__linux__)
void imgui_overlay_opengl3_init();
#endif
